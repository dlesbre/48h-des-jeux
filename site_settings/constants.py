# The website version number
WEBSITE_VERSION = "3.1.0"
WEBSITE_VERSION_DATE = "2024-08-31"

WEBSITE_FULL_VERSION = "{} - {}".format(WEBSITE_VERSION, WEBSITE_VERSION_DATE)

# Update this to force reload of cached css
CSS_VERSION = "1.3"
